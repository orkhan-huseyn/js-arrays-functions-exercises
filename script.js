/**
 * 1. sum adlı funksiya yazın.
 * Funksiya ədədlərdən ibarət bir arrayi parametr kimi qəbul etməli
 * və arrayın bütün elementlərinin cəmini qaytarmalıdır.
 */
function sum(array = []) {
  let result = 0;
  for (let i = 0; i < array.length; i++) {
    result += array[i];
  }
  return result;
}

/**
 * 2. random adlı funksiya yazın. Funksiya lower və upper adlı iki parametr qəbul etməli və
 * həmin iki ədəd arasında (hər ikisi daxil olmlaqla) ixtiyari bir natural ədəd qaytarmalıdır.
 */
function random(lower, upper) {
  return Math.floor(Math.random() * upper - lower) + lower;
}

/**
 * 3. arithmeticMean adlı funksiya yazın. Funksiyanız ədədlərdən ibarət bir arrayi parametr kimi qəbul
 * etməli və arrayin elementlərinin ədədi ortasını qaytarmalıdır.
 */
function arithmeticMean(numbers = []) {
  return sum(numbers) / numbers.length;
}

/**
 * 4. geometricMean adlı funksiya yazın. Funksiyanız ədədlərdən ibarət bir arrayi
 * parametr kimi qəbul etməli və arrayin elementlərinin həndəsi ortasını qaytarmalıdır.
 */
function geometricMean(numbers = []) {
  let product = 1;
  for (let i = 0; i < numbers.length; i++) {
    product *= numbers[i];
  }
  return Math.pow(product, 1 / numbers.length);
}

/**
 * 5. euclideanDistance adlı funksiya yazın. Funksiya iki vektoru parametr kimi qəbul etməli
 * və onlar arasındakı Evklid məsafəsini qaytarmalıdır.
 * Arrayin birinci elementi x ikinci elementi isə yi bildirir: [x, y].
 */
function euclideanDistance(vector1, vector2) {
  const x1 = vector1[0];
  const y1 = vector1[1];

  const x2 = vector2[0];
  const y2 = vector2[1];

  const xDiff = x2 - x1;
  const yDiff = y2 - y1;

  return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
}

/**
 * 6. pickOne adlı funksiya yazın. Funksiya bir ədəd array qəbul etməli
 * və içərisindən ixtiyari bir elementi qaytarmalıdır.
 */
function pickOne(array = []) {
  const randomIndex = random(0, array.length - 1);
  return array[randomIndex];
}

/**
 * 7. includes adlı funksiya yazın.
 * Funksiya bir ədəd array və bir ədəd axtarış üçün dəyər qəbul etməlidir.
 * Əgər həmin dəyər arrayin içərisində mövcuddursa, true əks halda false qaytarmalıdır.
 */
function includes(array = [], target) {
  for (let i = 0; i < array.length; i++) {
    if (array[i] == target) return true;
  }
  return false;
}

/**
 * 8. unique adlı funksiya yazın. Funksiya bir ədəd arrayi parametr kim qəbul etməli,
 * içərisində təkrar elementlər olmayan yeni bir array qaytarmalıdır
 */
function unique(array = []) {
  const result = [];

  for (let i = 0; i < array.length; i++) {
    const currentItem = array[i];
    const restOfArray = array.slice(i + 1);

    if (!includes(restOfArray, currentItem)) {
      result.push(currentItem);
    }
  }

  return result;
}

/**
 * 9. intersection adlı funksiya yazın. Funksiya iki arrayi parametr kimi qəbul etməli
 * və onların kəsişməsini yeni array olaraq qaytarmalıdır
 */
function intersection(array1, array2) {
  const result = [];

  for (let i = 0; i < array1.length; i++) {
    const currentItem = array1[i];

    if (includes(array2, currentItem)) {
      result.push(currentItem);
    }
  }

  return result;
}

/**
 * 10. flat2D adlı funksiya yazın. Funksiya bir ədəd iki ölçülü arrayi
 * parametr kimi qəbul etməli və bir ölçülü yeni bir array qaytarmalıdır
 */
function flat2D(array) {
  const flattenedArray = [];

  for (let i = 0; i < array.length; i++) {
    for (let j = 0; j < array.length; j++) {
      flattenedArray.push(array[i][j]);
    }
  }

  return flattenedArray;
}

/**
 * 11. union adlı funksiya yazın. Funksiya iki arrayi parametr kimi qəbul etməli
 * və onların birləşməsini yeni array olaraq qaytarmalıdır
 */
function union(array1, array2) {
  const result = [];

  for (let i = 0; i < array1.length; i++) {
    result.push(array1[i]);
  }

  for (let i = 0; i < array2.length; i++) {
    result.push(array2[i]);
  }

  return result;
}

/**
 * 12. reverse adlı funksiya yazın. Funksiya bir ədəd arrayi parametr kimi qəbul etməli
 * və onun tərsini yeni bir array kimi qaytarmalıdır
 */
function reverse(array) {
  const reversed = [];
  for (let i = array.length - 1; i >= 0; i--) {
    reversed.push(array[i]);
  }
  return reversed;
}

/**
 * 19. generatePassword adlı funksiya yazın. Funksiya passwordLength adlı bir parametr götürməli
 * və həmin uzunluqda şifrə generasiya etməlidir.
 * Şifrə rəqəmlərdən, böyük və kiçik ingilis hərflərindən ibarət olmalıdır
 */
function generatePassword(length) {
  const alphabet =
    'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM012345678';

  let password = '';

  for (let i = 0; i < length; i++) {
    let randomIndex = random(0, alphabet.length);
    let randomCharacter = alphabet[randomIndex];

    password += randomCharacter;
  }

  return password;
}
